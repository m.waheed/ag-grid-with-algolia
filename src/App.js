import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import {AllModules, LicenseManager} from 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

LicenseManager.setLicenseKey(
    'CompanyName=INDOS Corp.,LicensedApplication=garment.io,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=1,LicensedProductionInstancesCount=1,AssetReference=AG-010468,ExpiryDate=28_October_2021_[v2]_MTYzNTM3NTYwMDAwMA==8be036de53287b4b1516215f023992c4'
);

const App = () => {
    const columns = [
        {
            headerName: 'name',
            field: 'name'
        },
        {
            headerName: 'color',
            field: 'color'
        },
        {
            headerName: 'actions',
            field: 'actions'
        }
    ];

    // const datasource = {
    //   getRows(params) {
    //     console.log(JSON.stringify(params.request, null, 1));
    //     const { startRow, endRow, filterModel, sortModel } = params.request;

    //     // let url = `http://localhost:4000/olympic?`;

    //     // //Sorting
    //     let sorting = [];
    //     if (sortModel.length) {
    //       const { colId, sort } = sortModel[0];
    //       sorting = [`${sort}(${colId})`];

    //       index.setSettings({
    //         customRanking: sorting,
    //         ranking: [
    //           sorting[0],
    //           "typo",
    //           "geo",
    //           "words",
    //           "filters",
    //           "proximity",
    //           "attribute",
    //           "exact",
    //           "custom"
    //         ],
    //         'relevancyStrictness': 0
    //       }).then(() => {
    //         index.search('',{
    //           offset: startRow,
    //           length: endRow - startRow,
    //           facetFilters: filterKeys.map(filter => `${filter}:${filterModel[filter].filter}`),
    //         }).then(({hits})=> {
    //           console.log(hits)
    //           params.successCallback(hits)
    //         })
    //       });
    //     }

    //     //Filtering
    //     const filterKeys = Object.keys(filterModel);

    //     // //Pagination
    //     // url += `_start=${startRow}&_end=${endRow}`;

    //     // fetch(url)
    //     //   .then(httpResponse => httpResponse.json())
    //     //   .then(response => {
    //     //     console.log(response);
    //     //     params.successCallback(response, 500);
    //     //   })
    //     //   .catch(error => {
    //     //     console.error(error);
    //     //     params.failCallback();
    //     //   })

    //     // index.search('',{
    //     //   offset: startRow,
    //     //   length: endRow - startRow,
    //     //   facetFilters: filterKeys.map(filter => `${filter}:${filterModel[filter].filter}`),
    //     // }).then(({hits})=> params.successCallback(hits));
    //   }
    // };
    const datasource = {
        getRows(params) {
            console.log(JSON.stringify(params.request, null, 1));
            const { startRow, endRow, filterModel, sortModel } = params.request;
            let url = 'http://localhost:4000/colors?';
            //Sorting
            if (sortModel.length) {
                const { colId, sort } = sortModel[0];
                url += `_sort=${colId}&_order=${sort}&`;
            }
            //Filtering
            const filterKeys = Object.keys(filterModel);
            filterKeys.forEach(filter => {
                url += `${filter}=${filterModel[filter].filter}&`;
            });
            //Pagination
            url += `_start=${startRow}&_end=${endRow}`;
            fetch(url)
                .then(httpResponse => httpResponse.json())
                .then(response => {
                    console.log('response: ', response);
                    params.successCallback(response);
                })
                .catch(error => {
                    console.error(error);
                    params.failCallback();
                });
        }
    };
    const onGridReady = params => {
        // register datasource with the grid
        params.api.setServerSideDatasource(datasource);
    };

    return (
        <div style={{ margin: '30px' }}>
            <h1 align='center'>React-App</h1>
            <h4 align='center'>
                Implement Server-Side Pagination, Filter and Sorting in ag Grid
            </h4>
            <div className='ag-theme-alpine'>
                <AgGridReact
                    columnDefs={columns}
                    pagination={true}
                    domLayout='autoHeight'
                    modules={AllModules}
                    rowModelType='serverSide'
                    paginationPageSize={10}
                    cacheBlockSize={10}
                    onGridReady={onGridReady}
                    defaultColDef={{
                        filter: true,
                        floatingFilter: true,
                        sortable: true
                    }}
                />
            </div>
        </div>
    );
};
export default App;
